/******************************************************************************************
 * Data Structures in C++
 * ISBN: 7-302-33064-6 & 7-302-33065-3 & 7-302-29652-2 & 7-302-26883-3
 * Junhui DENG, deng@tsinghua.edu.cn
 * Computer Science & Technology, Tsinghua University
 * Copyright (c) 2006-2013. All rights reserved.
 ******************************************************************************************/

#pragma once

/******************************************************************************************
 * ��vector��������ʵ�ֲ��֣���������vector.h
 * Ч����ͬ�ڽ���Щʵ��ֱ�ӻ���vector.h
 * ��export��δ��������֧��ǰ����˿ɽ�������ʵ�ַ��룬�Ա�γ̽���
 ******************************************************************************************/
#include "../_share/release.h"
#include "../_share/util.h"

#include "vector_bracket.h"
#include "vector_assignment.h"

#include "vector_constructor_by_copying.h"

#include "vector_expand.h"
#include "vector_shrink.h"

#include "Vector_insert.h"
#include "vector_remove.h"
#include "vector_removeInterval.h"

#include "Vector_disordered.h"
#include "Vector_find.h"
#include "Vector_search.h"
#include "Vector_search_binary_C.h" //��A��B��C���ְ汾��C������
#include "Vector_search_fibonaccian_B.h" //��A��B���ְ汾��B�仪����A��ʵ�ã�������ֵ�д���ӿ�ͳһ��

#include "Vector_traverse.h"

#include "Vector_unsort.h"
#include "Vector_sort.h"
#include "vector_bubble.h"
#include "vector_bubbleSort.h"
#include "vector_selectionSort.h"
#include "vector_merge.h"
#include "vector_mergeSort.h"
#include "vector_partition_B.h" //��A��A1��B��B1��C��5���汾
#include "vector_quickSort.h"
#include "vector_heapSort.h"

#include "Vector_uniquify.h" //����Vector_uniquify_slow.h
#include "Vector_deduplicate.h"
